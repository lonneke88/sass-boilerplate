var convert = require('xml-js');

function handleData(xml) {
    var data = JSON.parse(convert.xml2json(xml, {compact: true, spaces: 4})).klantenvertellen;

    var filteredData = data.resultaten.resultaat.map(review => {
        return {
            name: review.antwoord[2]._cdata,
            city: review.antwoord[3]._cdata,
            message: review.antwoord[6]._cdata,
            rating: review.antwoord[7]._text,
        }
    });

    filteredData.forEach(item => {
        var {name, city, message, rating} = item;

        console.log(item);

        if (message) {
            var reviewTemplate = `<div class="slider-item">
                                    <div class="slider-item__rating">
                                        ${rating}
                                    </div>
                                    <div class="slider-item__content">
                                        <h3 class="slider-item__title">${name} <span class="slider-item__city"> uit ${city}</span></h3>
                                        <div class="slider-item__text">${message}</div>
                                    </div>
                                  </div>`;

            $('.slider--text').append(reviewTemplate);
        }

    });

    $('.slider--text').slick({
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

}

var jqxhr = $.get( "https://www.klantenvertellen.nl/xml/loodgieters_en_installateurs_jonker_bv/1", function(data) {
    var xmlstr = data.xml ? data.xml : (new XMLSerializer()).serializeToString(data);
    handleData(xmlstr)
})
  .fail(function() {
    console.log("Failure");
  });