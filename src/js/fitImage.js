var blocks = $('.block');
var prev;

function searchImage(elements) {
  elements.each(function (i) {

    if ($(this).attr('class') == prev && $(this).attr('class').indexOf('--image') >= 0) {
      elements.eq(i - 1).addClass('half');
    }

    prev = $(this).attr('class');

  });
}

searchImage(blocks);
