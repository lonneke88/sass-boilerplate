function checkIfEmpty(el) {
  el.each(function() {
    if (!/\S/.test($(this).html())) {
      $(this).parent().addClass('empty');
    }
  })
}

checkIfEmpty($('.block .block__content'));
