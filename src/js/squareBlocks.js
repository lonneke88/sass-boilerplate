function checkSize() {
  if ($(document).width() > 800) {
    if ($('.block').eq(0).height() < $('.block').eq(0).width()) {
      var blocks = $('.block:not(.block--fullwidth)');

      function fitBlocks() {
        blocks.each(function () {
          var block = $(this);
          block.height(block.width());
        });
      }

      $( window ).resize(function() {
        fitBlocks();
      });

      fitBlocks();

    }
  }
}

$( window ).resize(function() {
  checkSize();
});

checkSize();
