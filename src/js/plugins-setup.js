// slick
if ($('.slider--photo')) {
    $('.slider--photo').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

if ($('.popup-item')) {
    $('.popup-item').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: true
        }
    });
}

if ($('.projects__projects').length) {
    var mixer = mixitup('.projects__projects', {
        selectors: {
            target: '.filter-item'
        },
        animation: {
            effectsIn: 'fade',
            effectsOut: 'fade',
            duration: 200
        }
    });
}
