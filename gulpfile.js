const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    babel = require('gulp-babel');
const paths = {
    scss: './src/sass/style.scss',
    js: './src/js/**/*'
};
gulp.task('sass', () => {
    return gulp.src(paths.scss)
        .pipe(sass({
            includePaths: ['styles'].concat()
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest('./dist/css/'));
});
gulp.task('minifyJS', () => {
    return gulp.src(paths.js)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('index.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js/'));
});
gulp.task('default', ['sass', 'minifyJS'], () => {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['minifyJS']);
});
